package helpers
def call(){
    def aws_ci, aws_node_label
    repoBranch="master"
    repo = "${env.workflowRepo}"
    credentials_id="${env.devopsCredentialId}"
    aws_node_label="${env.aws_general_label}"

    def repoParams = [ type : 'global']

    // Carga del pipeline desde bitbucket
    fileLoader.withGit(repo, repoBranch, credentials_id, aws_node_label) {
        sh 'env'
        aws_ci = fileLoader.load('src/ci/common/ci');
        aws_ci.ci_flow(repoParams)
    }
}
